import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class Main {

    // Complete the birthday function below.
    static int birthday(List<Integer> s, int d, int m) {
        int sum=0;
        int result=0;
        if(s.size()<2)
            return 1;
        else {

            for (int i = 0; i <= s.size() - m; i++) {
                for (int j = i; j < m + i; j++) {
                    sum += s.get(j);
                }
                if (sum == d) {
                    result++;
                }
                //s.remove(0);
                sum = 0;

            }
        }

        return result;
    }

    public static void main(String[] args) throws IOException {

        List<Integer> s = new ArrayList<>();
        s.add(2);
        s.add(5);
        s.add(1);
        s.add(3);
        s.add(4);
        s.add(4);
        s.add(3);
        s.add(5);
        s.add(1);
        s.add(1);
        s.add(2);
        s.add(1);
        s.add(4);
        s.add(1);
        s.add(3);
        s.add(3);
        s.add(4);
        s.add(2);
        s.add(1);

        int d = 18;
        int m = 7;

        int result = birthday(s, d, m);

        System.out.println(result);

    }
}
